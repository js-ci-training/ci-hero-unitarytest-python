**Author**: Jacques Saraydaryan, All rights reserved
# Integration Continue et tests unitaires appliqués aux projets Python

## 0 Continuous Integration CI Continuous Deployment
Ci-dessous 2 schémas issus de la documentation de gitlab représentant un cycle de CI et CD:

![https://gitlab.cn/docs/14.0/ee/ci/introduction/img/gitlab_workflow_example_11_9.png](https://gitlab.cn/docs/14.0/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)


![https://gitlab.cn/docs/14.0/ee/ci/introduction/img/gitlab_workflow_example_extended_v12_3.png](https://gitlab.cn/docs/14.0/ee/ci/introduction/img/gitlab_workflow_example_extended_v12_3.png)

## 1 Contexte
L'objectif de ce tutoriel est de mettre en place des tests unitaires simples et de réaliser une librairie à partir d'un projet python.
Le processus de développement se fera au travers d'une intégration continue à l'aide de GitLab CI/CD. Le CI/CD va permettre d'automatiser la vérification de qualité de code (syntaxe), la vérification de non régression (tests unitaires) et la génération automatique d'une librairie python associée au projet.

## 2 Création des premiers éléments du projet
- Créer un repo GitLab ```test-python-ci-hero``` vide (à l'exception d'un fichier ```README.md``` et ```.gitignore``` que vous aller créer)
- Cloner votre repo. sur votre machine
  ```
  $ git clone https://gitlab.com/<your gitlab account>/test-python-ci-hero.git
  ```
- Créer un package python ```main``` (dossier qui contient un fichier ```__init__.py``` vide) 
- Créer un package python ```model``` à l'intérieur de ```main```
- Créer un ficher ```Hero.py``` qui va contenir le code métier
- Votre répertoire devrait avoir la forme suivante:

```
README.md
.gitignore
main
    |-__init__.py
    |-model
           |-__init__.py
           |- Hero.py
```
- Modifier le fichier ```Hero.py``` afin qu'il contienne le code suivant:

```python

class Hero:

    def __init__(self, name):
        self._name = name
        self._super_power = None
        self._elemental_affinity = None
        self._elemental_vulnerability = None
        self._hp = None
        self._energy = None
        self._attack = None
        self._attack_variance = None
        self._defense = None
        self._defense_std = None

    def set_all_params(self, name, super_power, elemental_affinity,  elemental_vulnerability, hp, energy, attack,
                       attack_variance, defense,  defense_std):
        self._name = name
        self._super_power = super_power
        self._elemental_affinity = elemental_affinity
        self._elemental_vulnerability = elemental_vulnerability
        self._hp = hp
        self._energy = energy
        self._attack = attack
        self._attack_variance = attack_variance
        self._defense = defense
        self._defense_std = defense_std

    def get_attack(self):
        return self._attack

    def get_defense(self):
        return self._defense

    def set_attack(self, attack):
        self._attack = attack

    def set_defense(self, defense):
        self._defense = defense

    ... les autres accesseurs ...
```

## 3 Création d'une première classe de Tests Unitaires

### 3.1 Répertoire de tests 

Créer un répertoire à la racine ```test``` qui va contenir la même arborescence de répertoire que ```main```. Créer un fichier ```Simple_test.py``` dans ```test/model```.
- Votre répertoire devrait avoir la forme suivante:

```
README.md
.gitignore
main
    |-__init__.py
    |-model
           |-__init__.py
           |- Hero.py
test
    |-__init__.py
    |-model
           |-__init__.py
           |- Simple_test.py
```

### 3.2 Fichier de tests basics
- Modifier le fichier ```Simple_test.py``` comme suit:

```python
import unittest
from main.model.Hero import Hero


class SimpleTestHero(unittest.TestCase):

    def test_hero_creation(self):
        h=Hero("test")
        self.assertIsNotNone(h)

    def test_hero_name_init(self):
        h = Hero("test")
        self.assertEqual(h.get_name(), "test")


if __name__ == '__main__':
    unittest.main()
```

- Analyse du code

```python
import unittest
``` 

- Permet d'importer les outils des tests ```unittest``` dans notre fichier

```python
class SimpleTestHero(unittest.TestCase):
```
- Notre classe va hériter des propriétés de la classe ```unittest.TestCase``` ce qui lui permettra d'utiliser des fonctions et décorateurs spécifiques pour nos tests. Cette déclaration va permettre de définir notre classe comme un ```scenario``` de test.

```python
...
def test_hero_creation(self):
...
def test_hero_name_init(self):
...
```
- Les méthodes présentées seront des tests individuels, le framework unittest va les détecter en se basant sur une convention de nommage --> toutes les méthodes commençant par le mot clé ```test```

```python
...
self.assertFalse(h, None)
...
self.assertEqual(h.get_name(), "test")
```
- Chaque méthode de tests doit faire appel à une fonction ```assert``` qui va définir le comportement attendu du test

### 3.3 Lancement du test

#### 3.3.1 IDE

La plupart des IDE permettent de lancer les tests directement.
E.g dans pycharm, clic droit sur le fichier de test ```Simple_test.py``` > run Unittest ...

```
Ran 2 tests in 0.001s

OK

Process finished with exit code 0
```

#### 3.3.2 Utiliser en ligne de commande avec pytest
L'utilitaire pytest va permettre de lancer tous les tests en respectant la convention de nommage des fichiers ```_test``` en suffixe ou ```test_ ``` en préfixe. 

A la racine du repository, lancer la commande suivante:
```
$ pytest
```
- :bulb: Sous Windows ou Mac il sera peut-être nécessaire d'appliquer la commande suivante:
  ```
  $ python -m pytest
  ```

L'utilitaire va chercher les fichiers dans l'ensemble des dossiers et exécuter les tests au besoin.

```
rootdir: \ci-python\ci-hero-unitarytest-python
collected 2 items                                                                                                                                                                                      
test\model\Simple_test.py ..                                                                                                                                                                               [100%]

=============================================================================================== 2 passed in 0.43s ===============================================================================================

```

En cas de problème de lancement (e.g. "no module name main found"), il faut mettre à jours la variable PYTHONPATH de votre terminal comme suit:

```
export PYTHONPATH="${PYTHONPATH}:/path/to/your/project/"
```

## 4 Création d'une classe de Tests unitaires plus avancée

### 4.1 Création de la nouvelle classe de tests
- Créer un nouveau fichier ``` Hero_test.py ``` dans le répertoire ```test/model```.
- Ajouter le contenu suivant au fichier

```python
import unittest
from main.model.Hero import Hero


class HeroTest(unittest.TestCase):
    heroTmp = ""

    @classmethod
    def setUpClass(cls):
        """Preparing the current TestClass, launch one time before the TestClass start"""
        print("--Preparing the current TestClass")
        cls.heroTmp = Hero("HERO_TMP")

    @classmethod
    def tearDownClass(cls):
        """Clearing the current TestClass, launch one time after the TestClass end"""
        print("--Clearing the current TestClass \n")
        cls.heroTmp = None

    def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method \n")
        if self.heroTmp._name != "HERO_TMP":
            self.heroTmp = Hero("HERO_TMP")

    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")

    def test_create_hero(self):
        print("------ ------ test_create_hero TEST \n")
        self.heroTmp = Hero("ME")
        self.assertIsNotNone(self.heroTmp)

    def test_all_params(self):
        print("------ ------ test_all_params TEST \n")
        self.heroTmp.set_all_params("ME1","super pink","water","fire",100,50,20,3,10,1)
        self.assertEqual(self.heroTmp._name,"ME1"," name not set correctly")
        self.assertEqual(self.heroTmp._super_power, "super pink", " superpower not set correctly")
        self.assertEqual(self.heroTmp._elemental_affinity, "water", " elementalAffinity not set correctly")
        self.assertEqual(self.heroTmp._elemental_vulnerability, "fire", " elementalVulnerability not set correctly")
        self.assertEqual(self.heroTmp._hp, 100, " hp not set correctly")
        self.assertEqual(self.heroTmp._energy, 50, " energy not set correctly")
        self.assertEqual(self.heroTmp._attack, 20, " attack not set correctly")
        self.assertEqual(self.heroTmp._attack_variance, 3, " attackVariance not set correctly")
        self.assertEqual(self.heroTmp._defense, 10, " defense not set correctly")
        self.assertEqual(self.heroTmp._defense_std, 1, " defenseStd not set correctly")


if __name__ == '__main__':
    unittest.main()

```

- Analyse du code
```python
import unittest
from main.model.Hero import Hero


class HeroTest(unittest.TestCase):
...
```
-   Comme précédemment les outils de unittest sont importés et notre classe hérite de ```unittest.TestCase```

```python
...
@classmethod
    def setUpClass(cls):
        """Preparing the current TestClass, launch one time before the TestClass start"""
        print("--Preparing the current TestClass")
        cls.heroTmp = Hero("HERO_TMP")
...
```
-   ```@classmethod``` définit que la méthode qui suit est propre à la classe et non à l'instance de la classe (cf https://steemit.com/utopian-io/@raptorjesus/python-7-la-difference-entre-staticmethod-et-classmethod pour plus d'information)

-   ```def setUpClass(cls)``` cette méthode va permettre de préparer tous les éléments nécessaires avant d'effectuer les tests du scenario de tests courant ```HeroTest```. En d'autres termes, cette méthode s'exécutera 1 fois au lancement du scenario de tests  ```HeroTest```

```python
...
    @classmethod
    def tearDownClass(cls):
        """Clearing the current TestClass, launch one time after the TestClass end"""
        print("--Clearing the current TestClass \n")
        cls.heroTmp = None
...
```
-   ```@classmethod``` idem que précédemment 
-   ```def tearDownClass(cls)``` cette méthode va nettoyer tous les éléments nécessaires à la fin de l'exécution de l'ensemble des tests du scénario de tests ```HeroTest```. En d'autres termes, cette méthode s'exécutera à la fin du scénario de tests ```HeroTest```.

```python
...
def setUp(self):
        """ Preparing current test method, launch before each test function"""
        print("------Preparing the current test method \n")
        if self.heroTmp._name != "HERO_TMP":
            self.heroTmp = Hero("HERO_TMP")
...
```
- ``` def setUp(self): ``` cette méthode permet de préparer tous les éléments nécessaires avant l'exécution de **CHAQUE TEST** 

```python
...
    def tearDown(self):
        """ Cleaning current test method results, launch after each test function"""
        print("------Cleaning the current test method \n")
...
```
- ```def tearDown(self):``` cette méthode permet de nettoyer l'ensemble des éléments nécessaires à la fin de l'exécution de **CHAQUE TEST**.

### 4.2 Exécution de la nouvelle classe de test
- Exécuter les tests de la classe ```Hero_test.py ```


```

--Preparing the current TestClass

------Preparing the current test method
------------ test_all_params TEST
------Cleaning the current test method 

------Preparing the current test method
------------ test_create_hero TEST
------Cleaning the current test method 

--Clearing the current TestClass 


Ran 2 tests in 0.008s

OK

Process finished with exit code 0
```
- Comme expliqué précédemment la méthode ```setUpClass``` est appelée au démarrage du scénario de tests ```--Preparing the current TestClass```
- Ensuite la méthode ```setUp``` est appelée avant chaque test ```Preparing the current test method```
- La méthode ```tearDown``` est appelée à la fin de chaque test ```------Cleaning the current test method```
- Enfin la méthode ```tearDownClass ``` est appelée à la fin du scénario de tests ```--Clearing the current TestClass ```

## 5 Integration Continue
### 5.1 GitLab CI/CD

Git lab propose un gamme d'outils permettant la mise en place de processus automatique pour l'intégration continue (e.g vérification de la syntaxe, exécution de test unitaires, création de livrable...).
Une complète présentation de la CI/CD de gitlab est disponible ici [https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/). 
![GitlabCi-CD image]( https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)
*Workflow Gitlab CI-CD*

Pour mettre en place l'intégration continue dans un repository git lab, il suffit d'ajouter un fichier ```.gitlab-ci.yml``` à la racine du répository.
Ce fichier va permettre de configurer tout le process automatique d'intégration continue.

### 5.2 Présentation de  ```.gitlab-ci.yml```

```.gitlab-ci.yml``` contient l'ensemble des instructions permettant la mise en oeuvre de pipeline d'intégration continue. Un ```pipeline``` est une suite d'opérations (```jobs```) qui va être réalisée automatiquement dès qu'un déclencheur est activé (e.g push sur la branch DEV, MASTER). Ces ```jobs``` peuvent être exécutés en parallèle et regroupés en ```stage``` (étape regroupant plusieurs ```jobs```). Une complète présentation des ```pipelines``` et des ```stages``` est disponible ici [https://docs.gitlab.com/ee/ci/pipelines.html](https://docs.gitlab.com/ee/ci/pipelines/).   

- Créer un fichier ```.gitlab-ci.yml``` à la racine de votre projet comme suit:

```yaml 
image: "python:3.11"

before_script:
  - python --version
  - python -m pip install --upgrade pip 
  - pip install -r requirements.txt

stages:
  - myStage1
  - myStage2

myJobA:
  stage: myStage1
  script:
  - echo "Hello I am the job myJobA from the stage myStage1"

myJobB:
  stage: myStage2
  script:
  - echo "Hello I am the job myJobB from the stage myStage2"
```

- Analyse du code

```image: "python:3.11"``` : afin de réaliser les opérations automatiques Gitlab CI/CD va créer un environnement virtuel ou les étapes se réaliseront. Cet environnement sera un container ```docker```. Afin de choisir l'environnement approprié, cette première ligne spécifie quelle image docker le projet va utiliser (image provenant https://hub.docker.com/). Une fois, l'environnement virtuel créé, gitlab va cloner votre projet dans cette environnement.

```yaml
before_script:
  - python --version
  - python -m pip install --upgrade pip 
  - pip install -r requirements.txt
```
```before_script``` permet d'exécuter des scripts sur l'environnement virtuel créé (e.g vérifier la version de python, télécharger les dépendances du projet)

```yaml
stages:
  - myStage1
  - myStage2
```

```stages``` définit l'ensemble des groupes d'opérations à effectuer ainsi que leur ordre. Ici le groupe d'opérations ```myStage1``` sera exécuté en premier puis ```myStage2``` ensuite.

```yaml
myJobA:
  stage: myStage1
  script:
  - echo "Hello I am the job myJobA from the stage myStage1"

myJobB:
  stage: myStage2
  script:
  - echo "Hello I am the job myJobB from the stage myStage2"
```

```myJobA```,```myJobB``` représente les jobs à effectuer. Il indique à quelle phase du ```pipeline``` ils devront être joués (e.g ```stage: myStage1```) ainsi que la liste des commandes a lancer (```script```)

### 5.3 Vérification de  ```.gitlab-ci.yml```
GitLab met à disposition un outil permettant de vérifier la syntaxe de votre fichier de configuration ```.gitlab-ci.yml```.
Vous le trouverez à l'URL suivante ```https://gitlab.com/<your repository name>/-/ci/lint```

- Vérifier le fichier ```.gitlab-ci.yml``` précédement créé.
- Pusher sur la branche master ```.gitlab-ci.yml```.
- Dans le Menu de Git-Lab > GI/CD > pipelines vous devriez voir les étapes de l'intégration continue se dérouler.


### 5.4 Modification de  ```.gitlab-ci.yml```: vérification de la syntaxe
Maintenant que nous avons présenté le fichier de configuration de la CI, nous allons l'adapter afin d'avoir un ```stage``` permettant de vérifier que notre code possède une syntawe correcte.

- Modifier le fichier ```.gitlab-ci.yml``` comme suit:

```yaml
image: "python:3.11"

before_script:
  - python --version
  - python -m pip install --upgrade pip
  - pip install -r requirements.txt

stages:
  - Static Analysis

flake8:
  stage: Static Analysis
  script:
  - flake8 --max-line-length=150 ./main
```

Dans le fichier de configuration créé nous allons:
- Utiliser un environement virtuel basé sur l'image docker python:3.11 (```image: "python:3.11"```)
- Mettre à jour les dépendances python (```before_script:```)
- Définir un stage **Static Analysis** (```stages:```)
- Définir un job **flake8** qui va faire parti du stage **Static Analysis** (```flake8:```)

**flake8** est un utilitaire qui va permettre de vérifier la syntaxe de votre code python. Ici seul les fichiers contenus dans ./main seront vérifiés. Vous trouverez une information complète sur flake8 [ici](https://medium.com/python-pandemonium/what-is-flake8-and-why-we-should-use-it-b89bd78073f2).

- Vérifier votre fichier ``.gitlab-ci.yml``` à l'aide de l'outil gitlab ```https://gitlab.com/<your repository name>/-/ci/lint```.
- Commiter vos changements
- Vérifier que vos tests se passent bien (corriger votre code en conséquence)

### 5.5 Modification de  ```.gitlab-ci.yml```: execution de tests untaires
Afin de vérifier le bon fonctionnement de l'application en cours, nous devons lancer des tests de non-régression. Pour ce faire nous allons définir un nouveau ```stage``` **Test**.
- Modifier le fichier ```.gitlab-ci.yml``` comme suit:
```yaml
image: "python:3.11"

before_script:
  - python --version
  - python -m pip install --upgrade pip
  - pip install -r requirements.txt

stages:
  - Static Analysis
  - Test

flake8:
  stage: Static Analysis
  script:
  - flake8 --max-line-length=150 ./main

pytest:
  stage: Test
  script:
  - pytest
```

Dans le fichier de configuration nous avons ajouté un ```stage``` **Test**. Un job **pytest** appartenant au stage **Test** a également été ajouté. Ce dernier va executer l'ensemble des tests unitaires de l'application.

- Vérifier votre fichier ```.gitlab-ci.yml``` à l'aide de l'outil gitlab ``` https://gitlab.com/<your repository name>/-/ci/lint```.
- Commiter vos changements
- Vérifier que vos tests se passent bien (corriger votre code en conséquence)

### 5.5 Modification de  ```.gitlab-ci.yml```: création d'un livrable
L'intégration continue permet également de produire une application ou une librairie de l'état courant de notre répository. A l'issue de l'intégration continue, si toutes les étapes se sont bien déroulées nous pouvons demander à générer un livrable de notre application

#### 5.5.1 Création d'une libraire python  

Afin de distribuer la version de notre application python, nous devons créer une librairie python.
- Ajouter les fichiers suivants à la racine du repository
  - ```README.txt```
  - ```setup.py```
  
- Modifier le fichier ```setup.py```
  
```python
from distutils.core import setup

setup(
    name='MyHeroLib',
    version='0.1dev',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    author="John Doe",
    author_email="jdoe@company.com",
    description="A small example package",
    long_description=open('README.txt').read(),
    package_dir={"": "main"},
    packages=setuptools.find_namespace_packages(where="main"),
    python_requires='>=3.6',
)
```
- le fichier ```setup.py``` va permettre de définir les élements nécessaires pour la génération de la librairie relative à votre application
- les éléments qui constituent le ```setup.py``` sont définis ici https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project.

```python
...
  package_dir={"": "main"},
  packages=setuptools.find_namespace_packages(where="main"),
```
- Ici afin de ne pas ajouter le répertoire de tests à notre archive finale, nous spécifions où se positionnent les fichiers sources de notre projet


#### 5.5.2 Génération automatique d'une librairie source
Afin de créer un livrable automatiquement lors de l'exécution d'une pipeline, modifier le fichier ```.gitlab-ci.yml``` comme suit:

```yaml
image: "python:3.11"

before_script:
  - python --version
  - python -m pip install --upgrade pip
  - pip install -r requirements.txt

stages:
  - Static Analysis
  - Test
  - Lib generation

flake8:
  stage: Static Analysis
  script:
  - flake8 --max-line-length=150 ./main

pytest:
  stage: Test
  script:
  - pytest

source_dist:
  stage: Lib generation
  script:
  - python setup.py sdist
  artifacts:
    paths:
    - ./dist/*.tar.gz
    expire_in: 1 week
```

Un nouveau ```stage```, **Lib Generation** a été ajouté. Un nouveau job **source_dist** appartenant au stage **Lib Generation** a également été ajouté.

- Analyse du code

```yaml
...
source_dist:
  stage: Lib generation
  script:
  - python setup.py sdist
...
```

- ``` python setup.py sdist ``` va permettre de générer une librairie source de votre application python.


```yaml
...
  artifacts:
    paths:
    - ./dist/*.tar.gz
    expire_in: 1 week
```

- ```artifacts:``` permet de définir les éléments à récupérer dans l'environement virtuel si l'état en cours est un success.
- ```paths:``` définit l'ensemble des dossiers et/ou fichiers à récupérer
- ```expire_in:``` précise combien de temps seront gardés ces fichiers dans les serveurs gitlabs.


- Vérifier votre fichier ```.gitlab-ci.yml``` à l'aide de l'outil gitlab ``` https://gitlab.com/<your repository name>/-/ci/lint```.
- Commiter vos changements
- Vérifier que vos tests se passent bien et que les fichiers sélectionnés sont accessibles au téléchargement.


#### 5.5.2 Génération automatique d'une librairie binaire
Afin de créer un livrable automatiquement lors de l'exécution d'un pipeline, modifier le fichier ```.gitlab-ci.yml``` comme suit:

```yaml
image: "python:3.11"

before_script:
  - python --version
  - python -m pip install --upgrade pip
  - pip install -r requirements.txt

stages:
  - Static Analysis
  - Test
  - Lib generation

flake8:
  stage: Static Analysis
  script:
  - flake8 --max-line-length=150 ./main

pytest:
  stage: Test
  script:
  - pytest

source_dist:
  stage: Lib generation
  script:
  - python setup.py sdist
  artifacts:
    paths:
    - ./dist/*.tar.gz
    expire_in: 1 week

bin_dist:
  stage: Lib generation
  script:
  - python setup.py bdist_wheel
  - ls ./dist/
  artifacts:
    paths:
    - ./dist/*.whl
    expire_in: 1 week
```

Un nouveau ```job```, **bin_dist**   a été ajouté au stage  **Lib Generation**. Ce job va s'exécuter en parallèle du job **source_dist** et va permettre de générer une version binaire de votre librairie python.

- Analyse du code

```yaml
...
bin_dist:
  stage: Lib generation
  script:
  - python setup.py bdist_wheel
...
```

- ``` python setup.py bdist_wheel ``` va permettre de générer une librairie binaire de votre application python au format ```*.whl`` .

- Vérifier votre fichier ```.gitlab-ci.yml``` à l'aide de l'outil gitlab ``` https://gitlab.com/<your repository name>/-/ci/lint```.
- Commiter vos changements
- Vérifier que vos tests se passent bien et que les fichiers sélectionnés sont accessibles au téléchargement.
- Vérifier également le pipeline final réalisé:

1. Static anaysis (flake8)
2. Test (pytest)
3. Lib generation(bin_dist, source_dist)

![Final Workflow](./images/final-worflow.png) 

## 5.6 Enregister la librairie Python dans le Package de Gitlab

Gitlab met à disposition des espaces de stockage pour des librairies java, python ou javascript. Cette fonctionnalité permet de mettre à disposition des libraries générées par les équipes à toutes les personnes ayant les droits sur le repo. ou à tout le mode si le repo. est publique

- modifier le fichier ```.gitlab-ci.yml`` comme suit:
  
```yaml
...

stages:
  - Static Analysis
  - Test
  - Lib generation
  - Deploy

...

deploy:
  stage: Deploy
  script:
    - python setup.py sdist bdist_wheel
    - pip install twine 
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

  only:
    - master
```

- Analyse du code

```yaml
  stages:
    ...
    - Deploy
```
  - `Deploy`  ajout d'un nouveau stage pour deployer la librairie python

```yaml
...
deploy:
  stage: Deploy
  script:
  ...
...
```
 - Contenu de l'étape de déploiement
 - `python setup.py sdist bdist_wheel` création de la librairie python
 - `pip install twine ` installation de la librairie `twine` utiliser pour déployer la librairie
```yaml
TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
```
  - `TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token` : définition des variables fournies par Gitlab permetteant d'utiliser l'utilitaire `package` de git lab.
  - `python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*` : upload de la librairie sur gitlab en utilisant les token associés

- Votre librairie est déployée, pour l'utiliser il suffit d'utiliser `pip` comme suit:

```
pip install MyHeroLib --extra-index-url https://gitlab.com/api/v4/projects/17048463/packages/pypi/simple

```



![Final Workflow](./images/DeployPipeLinev1.jpg) 

![Final Workflow](./images/DeployV1.jpg) 
![Final Workflow](./images/DeployV2.jpg) 


## 5.7 Ajouter des templates de Ci existants
- Gitlab propose une série de template d'action CI-CD permettant d'exécuter sur votre application des actions standards.

- Modifier votre fichier `.gitlab-ci.yml` comme suit pour ajouter le template de detection de vulnérabilités dans les dépendances python 

```yaml
...

stages:
  - Static Analysis
  - Test
  - Test-security
  - Lib generation
  - Deploy

...

sast:
  stage: Test-security
include:
- template: Security/SAST.gitlab-ci.yml

...

```

- Lors de cette étape, un rapport de sécurité sera disponible dans les artefacts , `gl-sast-report.json`

```json
{"version":"15.1.4","vulnerabilities":[],"scan":{"analyzer":{"id":"semgrep","name":"Semgrep","url":"https://gitlab.com/gitlab-org/security-products/analyzers/semgrep","vendor":{"name":"GitLab"},"version":"5.25.0"},"scanner":{"id":"semgrep","name":"Semgrep","url":"https://github.com/returntocorp/semgrep","vendor":{"name":"GitLab"},"version":"1.74.0"},"type":"sast","start_time":"2025-01-17T11:11:06","end_time":"2025-01-17T11:11:16","status":"success"}}
```



- ref https://docs.gitlab.com/ee/user/application_security/sast/
- ref https://docs.gitlab.com/ee/ci/yaml/#include

## 5.8 Sauvegarder les dépendances 
- lors du séquencement de plusieurs stages, il peut être intéressant de conserver des éléments plutôt que de les re-télécharger, compiler.
- Dans notre cas, il est intéressant de conserver les dépendances de requirement.txt entre les différents stages.

- Pour cela nous allons créer un environement virtuel en python et sauvegarder les dépendances télécharger dans artefact dans le premier stage

  ```yaml
    before_script:
      - python --version

    flake8:
      stage: Static Analysis
      script:
      - python -m pip install --upgrade pip
      - python -m venv my-venv
      - . my-venv/bin/activate
      - pip install -r requirements.txt
      - flake8 --max-line-length=150 ./main
    ...
  ```
  - Explications
    - `- python -m venv my-venv ` : création d'un environnement virtuel pour python `my-venv`
    - `- . my-venv/bin/activate `: activation de l'environnement virtuel préalablement créé.
    - `- pip install -r requirements.txt`: téléchargement des dépendances nécessaires dans cet environnement virtuel
  - Les artefacts sont automatiquement mis à disposition dans les autres stages
  - Dans les autres stages, nous allons activer le virtual env avec les dépendances déjà télécharger

  ```yaml
    ...
  
      pytest:
        stage: Test
        script:
        - . my-venv/bin/activate
        - pytest
  
      source_dist:
        stage: Lib generation
        script:
        - . my-venv/bin/activate
        - python setup.py sdist
        artifacts:
          paths:
          - ./dist/*.tar.gz
          expire_in: 1 week
    ...
  
  ```
  - Explication
    - `- . my-venv/bin/activate`: les dépendances sont déjà présentes dans l'environnement virtuel `my-venv`, il suffit d'activer cette environnement par cette commande.

  - le fichier `.gitlab-ci` obtenu doit être le suivant:

    ```yaml
      image: "python:3.11"
  
      before_script:
        - python --version
  
      stages:
        - Static Analysis
        - Test
        - Test-security
        - Lib generation
        - Deploy
  
  
      sast:
        stage: Test-security
      include:
      - template: Security/SAST.gitlab-ci.yml
  
      flake8:
        stage: Static Analysis
        script:
        - python -m pip install --upgrade pip
        - python -m venv my-venv
        - . my-venv/bin/activate 
        - pip install -r requirements.txt
        - flake8 --max-line-length=150 ./main
        artifacts:
          paths:
          - my-venv/
  
      pytest:
        stage: Test
        script:
        - . my-venv/bin/activate
        - pytest
  
      source_dist:
        stage: Lib generation
        script:
        - . my-venv/bin/activate
        - python setup.py sdist
        artifacts:
          paths:
          - ./dist/*.tar.gz
          expire_in: 1 week
  
      bin_dist:
        stage: Lib generation
        script:
        - . my-venv/bin/activate
        - python setup.py bdist_wheel
        - ls ./dist/
        artifacts:
          paths:
          - ./dist/*.whl
          expire_in: 1 week
  
      deploy:
        stage: Deploy
        script:
          - . my-venv/bin/activate
          - python setup.py sdist bdist_wheel
          - pip install twine 
          - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
  
        only:
          - master
          - dev
    ```


## 5.9 Aller plus loin dans dans GilabCI
 Une complète description du GitLab CI/CD est disponible ici https://docs.gitlab.com/ee/ci/README.html.
