
class Hero:

    def __init__(self, name):
        self._name = name
        self._super_power = None
        self._elemental_affinity = None
        self._elemental_vulnerability = None
        self._hp = None
        self._energy = None
        self._attack = None
        self._attack_variance = None
        self._defense = None
        self._defense_std = None

    def set_all_params(self, name, super_power, elemental_affinity,  elemental_vulnerability, hp, energy, attack,
                       attack_variance, defense,  defense_std):
        self._name = name
        self._super_power = super_power
        self._elemental_affinity = elemental_affinity
        self._elemental_vulnerability = elemental_vulnerability
        self._hp = hp
        self._energy = energy
        self._attack = attack
        self._attack_variance = attack_variance
        self._defense = defense
        self._defense_std = defense_std

    def get_name(self):
        return self._name

    def get_attack(self):
        return self._attack

    def get_defense(self):
        return self._defense

    def get_hp(self):
        return self._hp

    def get_energy(self):
        return self._energy

    def get_attack_variance(self):
        return self._attack_variance

    def get_defense_stp(self):
        return self._defense_std

    def get_elemental_affinity(self):
        return self._elemental_affinity

    def set_attack(self, attack):
        self._attack = attack

    def set_defense(self, defense):
        self._defense = defense

    def set_hp(self, hp):
        self._hp = hp

    def set_energy(self, energy):
        self._energy = energy

    def set_attack_variance(self, attack_variance):
        self._attack_variance = attack_variance

    def set_defense_stp(self, defense_stp):
        self._defense_std = defense_stp

    def set_elemental_affinity(self, elemental_affinity):
        self._elemental_affinity = elemental_affinity

    def set_name(self, name):
        self._name = name
